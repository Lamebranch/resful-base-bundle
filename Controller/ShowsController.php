<?php

namespace Lamebranch\Bundle\RestfulBaseBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ShowsController extends RestfulController
{
	/**
	 * @Rest\View
	 */
	public function getShowsAction()
	{

        $page = max(1,$this->getRequest()->get('page',1));

        $criteria = $this->getRequest()->get('criteria');
        $arrCriteria = (array) json_decode($criteria);

        $itemsPerPage = $this->container->getParameter('lamebranch_restful_base.default_limit');
//        if ( $this->container->hasParameter('itix_restful.sections.shows.limit') )



        $mongoShow = $this->get('qwad_mongo_db.manager')->getCollection('Shows');

        $_data = $mongoShow->find($arrCriteria);

        $totalcount = $_data->count();

        $lastpage = ceil($totalcount/$itemsPerPage);

        $_data->limit($itemsPerPage);

		$data=array();
	    foreach($_data as $_datarow) {
            $_datarow['links']['showinfo'] = 'http://'.$_SERVER['HTTP_HOST'].'/api/shows/'.$_datarow['_id'];
		    $data[] = $_datarow;

	    }


        $result = array(
            'data' => $data,
            'totalItems' => $totalcount,
        );
        if ( $totalcount > $itemsPerPage ) {
            if ( $page != $lastpage ) {
                $result['links']['next'] = 'http://'.$_SERVER['HTTP_HOST'].'/api/shows?page='.($page+1).($criteria!=''?'&criteria='.$criteria:'');
            }

            if ( $page > 1 ) {
                $result['links']['prev'] = 'http://'.$_SERVER['HTTP_HOST'].'/api/shows?page='.($page-1).($criteria!=''?'&criteria='.$criteria:'');
            }

        }


        return $result;
	}

	/**
	 * @Rest\View
	 */
	public function getShowAction($id)
	{
		$mongoShow = $this->get('qwad_mongo_db.manager')->getCollection('Shows');
		$data = $mongoShow->findById($id);

		if ( !$data )
			throw new NotFoundHttpException('Show not found');

		return array('data'=>$data);
	}
}
