<?php

namespace Lamebranch\Bundle\RestfulBaseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lamebranch_restful_base');

        $rootNode
		    ->children()
                ->integerNode('default_limit')
                    ->defaultValue(30)
                ->end()
//                ->arrayNode('sections')
//                    ->requiresAtLeastOneElement()
//                    ->useAttributeAsKey('name')
//                    ->prototype('array')
//                        ->children()
//                            ->integerNode('limit')->end()
//                            ->integerNode('max_limit')->end()
//                        ->end()
//                    ->end()
//                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
