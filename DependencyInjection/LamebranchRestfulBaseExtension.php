<?php

namespace Lamebranch\Bundle\RestfulBaseBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LamebranchRestfulBaseExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->addParams($container, $config, 'lamebranch_restful_base.');

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
    }

    private function addParams(ContainerBuilder $container, array $config, $name)
    {
        foreach($config as $key => $value)
        {
            if ( is_array($value) )
            {
                $this->addParams($container, $value, $name.$key.'.');
            }
            else
            {
                $container->setParameter($name.$key, $value);
            }
        }
    }
}
